package hr.fer.zemris.ims;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Random;

import mmaracic.gameaiframework.PacmanVisibleWorld;
import mmaracic.gameaiframework.WorldEntity;

public class GhostKM extends mmaracic.gameaiframework.AgentAI {

	private static final int TOLERIRAJ = 500;
	private static final int MOJA_TOLERANCIJA = 1500;
	private static final long INTERVAL = 5000;
	private static final int MAX = 1000000;
	private static int tMove = 20;
	private Date now = new Date();

	private int goRandom(ArrayList<int[]> moves, int[] staraPozicija) {
		int choice = 0;
		Random r = new Random(now.getTime());

		if (moves.size() > 1) {
			while (true) {
				choice = r.nextInt(moves.size());
				if (moves.get(choice)[0] == staraPozicija[0] && moves.get(choice)[1] == staraPozicija[1]) {
					continue;
				} else
					break;

			}
		}
		return choice;
	}

	class PacmanViden {
		public int[] potez;
		public long vrijeme;

		public PacmanViden(int[] potez, long vrijeme) {
			this.potez = potez;
			this.vrijeme = vrijeme;
		}
	}

	private int findClosest(ArrayList<int[]> moves, int[] pos) {
		int index = 0;
		int[] move = moves.get(0);
		float dist = Math.abs(pos[0] - move[0]) + Math.abs(pos[1] - move[1]);
		for (int i = 1; i < moves.size(); i++) {
			move = moves.get(i);
			float currDist = Math.abs(pos[0] - move[0]) + Math.abs(pos[1] - move[1]);
			if (currDist < dist) {
				dist = currDist;
				index = i;
			}
		}
		return index;
	}

	@Override
	public int decideMove(ArrayList<int[]> moves, PacmanVisibleWorld mySurroundings,
			WorldEntity.WorldEntityInfo myInfo) {


		int[] move = null;
		long timeM = -1000;
		int[] staraPozicija = new int[] { MAX, MAX };
		ArrayList<int[]> neposjeceno = new ArrayList<int[]>();

		ArrayList<int[]> vidjeliDrugiAgenti = new ArrayList<int[]>();

		for (int i = -mySurroundings.getDimensionX() / 2; i <= mySurroundings.getDimensionX() / 2; i++) {
			for (int j = -mySurroundings.getDimensionY() / 2; j <= mySurroundings.getDimensionY() / 2; j++) {
				ArrayList<WorldEntity.WorldEntityInfo> elements = mySurroundings.getWorldInfoAt(i, j);
				HashMap<Integer, Object> metaHash = mySurroundings.getWorldMetadataAt(i, j);

				ArrayList<Integer> RemoveList = new ArrayList<>();
				if (i == 0 && j == 0) {
					while (metaHash.containsKey(myInfo.getID()))
						metaHash.remove(myInfo.getID());

					// printStatus("Stavljam");
					metaHash.put(myInfo.getID(), new PacmanViden(new int[] { MAX, MAX }, System.currentTimeMillis()));
					continue;
				}

				if (metaHash != null && !metaHash.isEmpty()) {
					for (Integer id : metaHash.keySet()) {
						PacmanViden history = (PacmanViden) metaHash.get(id);
						long interval = System.currentTimeMillis() - history.vrijeme;
						if (interval > INTERVAL) // prošlo je dosta vremena, briši!!!
						{
							RemoveList.add(id);
						}
					}
				}

				for (Integer id : RemoveList) {
					metaHash.remove(id);
				}

				// find pacman
				if (elements != null && metaHash != null) {
					for (WorldEntity.WorldEntityInfo el : elements) {
						if (el.getIdentifier().compareToIgnoreCase("Pacman") == 0) {
							int index = findClosest(moves, new int[] { i, j });
							metaHash.clear();
							metaHash.put(myInfo.getID(), new PacmanViden(moves.get(index), System.currentTimeMillis()));
							return index;
						}
					}
					// Drugi vidi pacmana
					if (!metaHash.isEmpty()) {
						for (Integer id : metaHash.keySet()) {
							PacmanViden history = (PacmanViden) metaHash.get(id);

							if (id != myInfo.getID() && history.potez[0] < MAX && history.potez[1] < MAX) 
							{
								move = ((PacmanViden) metaHash.remove(id)).potez;
								for (int a = 0; a < moves.size(); a++) {
									int[] m = moves.get(a);
									if (m[0] == move[0] && m[1] == move[1]) {
										return a;
									}
								}
								// printStatus(myInfo.getID()+": Found pacman trail left by ghost: "+id+"!");
							}

						}
					}
				}

				if (metaHash != null && checkZO(i,j)) {
					boolean flag = true;
					for (Integer id : metaHash.keySet()) {
						PacmanViden history = (PacmanViden) metaHash.get(id);
						long interval = System.currentTimeMillis() - history.vrijeme;

						if (id == myInfo.getID()) // bio sam vec tu, zabiljezi
						{
							if (interval > timeM) {
								staraPozicija = new int[] { i, j, (int) interval };
								timeM = interval;
							}
							flag = false;
						}
					}

					if (flag) {
						for (Integer id : metaHash.keySet()) {
							// printStatus("moj id: "+myInfo.getID()+" for id: "+id);
							PacmanViden history = (PacmanViden) metaHash.get(id);
							long interval = System.currentTimeMillis() - history.vrijeme;

							if (id != myInfo.getID()) {
								vidjeliDrugiAgenti.add(new int[] { i, j, (int) interval });
								flag = false;
							}
						}

						if (flag) {
							// nevidljivo
							neposjeceno.add(new int[] { i, j });

						}
					}
				}

			}
		}
		if (tMove>0) {
			tMove--;
			return goRandom(moves, staraPozicija);
		}
		// Go where metadata pointed
		if (move != null) {
			for (int i = 0; i < moves.size(); i++) {
				int[] m = moves.get(i);
				if (m[0] == move[0] && m[1] == move[1]) {
					return i;
				}
			}
		}
		// ako postoji neposjećeno polje, idi na njega (biraj random od postojećih)
		if (neposjeceno.size() > 0) {
			int randIndex = new Random(now.getTime()).nextInt(MAX) % neposjeceno.size();

			for (int i = 0; i < moves.size(); i++) {
				int[] m = moves.get(i);
				if (m[0] == (neposjeceno.get(randIndex))[0] && m[1] == (neposjeceno.get(randIndex))[1]) {
					return i;
				}
			}
		}
		if (vidjeliDrugiAgenti.size() > 0) {
			int max_index = 0;

			for (int i = 0; i < vidjeliDrugiAgenti.size(); i++) {
				if (vidjeliDrugiAgenti.get(i)[2] > vidjeliDrugiAgenti.get(max_index)[2]) {
					max_index = i;
				}
			}
			if (vidjeliDrugiAgenti.get(max_index)[2] >= TOLERIRAJ) {
				for (int i = 0; i < moves.size(); i++) {
					int[] m = moves.get(i);
					if (m[0] == vidjeliDrugiAgenti.get(max_index)[0] && m[1] == vidjeliDrugiAgenti.get(max_index)[1]) {
						return i;
					}
				}
			}
		}
		if (staraPozicija[0] < MAX && staraPozicija[1] < MAX && staraPozicija[2] >= MOJA_TOLERANCIJA) {
			for (int i = 0; i < moves.size(); i++) {
				int[] m = moves.get(i);
				if (m[0] == staraPozicija[0] && m[1] == staraPozicija[1]) {
					return i;
				}
			}
		}

		// Go random
		return goRandom(moves, staraPozicija);
	}

	private boolean checkZO(int i, int j) {
		return (i == 0 && (j == 1 || j == -1) || j == 0 && (i == -1 || i == 1));
	}
}
